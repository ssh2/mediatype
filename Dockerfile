FROM openjdk:11
ADD target/media.jar media.jar
ENTRYPOINT ["java","-jar","media.jar"]
