package com.freelance.media.repository;

import com.freelance.media.model.post.Post;
import com.freelance.media.model.user.User;
import com.freelance.media.repository.provider.PostProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

  @InsertProvider(type = PostProvider.class,method = "addPost")
  @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
  Boolean addPost(Post post);

  @InsertProvider(type = PostProvider.class, method = "addPostImage")
  Boolean addPostImage(int postId, String imagePath);

  @SelectProvider(type = PostProvider.class, method = "newFeed")
  @Results({
          @Result(property = "userId",column = "user_id"),
          @Result(property = "isEdited",column = "is_edited")
  })
  List<Post> newFeed(int userId);
}
