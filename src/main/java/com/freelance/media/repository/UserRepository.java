package com.freelance.media.repository;

import com.freelance.media.model.user.User;
import com.freelance.media.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

  @SelectProvider(type = UserProvider.class, method = "getAllUsers")
  @Results({
          @Result(property = "userName",column = "user_name"),
          @Result(property = "displayName",column = "display_name"),
          @Result(property = "phoneNumber",column = "phone_number"),
          @Result(property = "imagePath",column = "image_path"),
          @Result(property = "createdDate",column = "created_date")
  })
  List<User> getAllUser();

  @SelectProvider(type = UserProvider.class, method = "getUserById")
  @Results({
          @Result(property = "userName",column = "user_name"),
          @Result(property = "displayName",column = "display_name"),
          @Result(property = "phoneNumber",column = "phone_number"),
          @Result(property = "imagePath",column = "image_path"),
          @Result(property = "createdDate",column = "created_date")
  })
  User getUserById(int id);

  @SelectProvider(type = UserProvider.class, method = "isUsedUsername")
  List<User> isUsedUsername(String username);

  @InsertProvider(type = UserProvider.class,method = "addNewUser")
  @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
  Boolean addNewUser(User user);

  @UpdateProvider(type = UserProvider.class,method = "updateUser")
  Boolean updateUser(User user, int id);
}
