package com.freelance.media.repository;

import com.freelance.media.model.connection.Following;
import com.freelance.media.model.post.Post;
import com.freelance.media.repository.provider.ConnectionProvider;
import com.freelance.media.repository.provider.PostProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository {

  @InsertProvider(type = ConnectionProvider.class,method = "follow")
  @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
  Boolean follow(Following following);

}
