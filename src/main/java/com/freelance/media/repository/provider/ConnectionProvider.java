package com.freelance.media.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class ConnectionProvider {

  public String follow(){
    return new SQL(){{
      INSERT_INTO("md_following");
      VALUES("follower", "#{follower}");
      VALUES("user_id", "#{user}");
    }}.toString();
  }
}
