package com.freelance.media.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class PostProvider {

  public String addPost(){
    return new SQL(){{
      INSERT_INTO("md_post");
      VALUES("user_id","#{userId}");
      VALUES("date","#{date}");
      VALUES("title","#{title}");
      VALUES("is_edited","#{isEdited}");
      VALUES("privacy","#{privacy}");
    }}.toString();
  }

  public String addPostImage(int postId, String imagePath){
    return new SQL(){{
      INSERT_INTO("md_post_image");
      VALUES("post_id","#{postId}");
      VALUES("image_path","#{imagePath}");
    }}.toString();
  }

  public String newFeed(int userId){
    return "SELECT * FROM md_post\n" +
            "WHERE privacy = 'public' OR user_id IN (\n" +
            "\tSELECT user_id FROM md_following WHERE md_following.follower = "+userId+")";
  }
}
