package com.freelance.media.controller.post;

import com.freelance.media.model.post.PostRequest;
import com.freelance.media.model.post.PostResponse;
import com.freelance.media.model.user.UserRequest;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.service.PostService;
import com.freelance.media.service.UserService;
import com.freelance.media.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = ApiConstant.baseUrl + "/posts")
public class PostController {

  PostService postService;
  UserService userService;
  Message message;
  MethodReducer methodReducer;
  Validate validate;

  @Autowired
  public void setPostService(PostService postService) {
    this.postService = postService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setValidate(Validate validate) {
    this.validate = validate;
  }

  @Autowired
  public void setMethodReducer(MethodReducer methodReducer) {
    this.methodReducer = methodReducer;
  }

  @Autowired
  public void setMessage(Message message) {
    this.message = message;
  }

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity addPost(@RequestBody PostRequest post){
    ResponseEntity<InvalidApiResponse> invalidResponse = checkPost(post);
    if (invalidResponse==null){
      PostResponse addedPost = postService.addPost(post);
      if (addedPost!=null){
        return ResponseEntity.ok(new BaseApiResponse<>(
                message.success(message.INSERTSUCCESS, "Post"),
                addedPost, HttpStatus.CREATED
        ));
      } else {
        return ResponseEntity.internalServerError().body(new InvalidApiResponse(
           message.INTERNALERROR, HttpStatus.INTERNAL_SERVER_ERROR
        ));
      }
    } else {
      return invalidResponse;
    }
  }

  @GetMapping(value = "/newfeed/{id}")
  public ResponseEntity newFeed(@PathVariable String id){
    if (validate.checkDigit(id)){
      System.out.println(id);
      UserResponse user = userService.getUserById(Integer.parseInt(id));
      System.out.println(user);
      if (user!=null){
        return ResponseEntity.ok(new BaseApiResponse<>(
           message.success(message.GETSUCCESS, "Posts"),
           postService.newFeed(Integer.parseInt(id)), HttpStatus.FOUND
        ));
      }
      return ResponseEntity.status(404).body(new InvalidApiResponse(
              message.notFound("User",Integer.parseInt(id)),HttpStatus.NOT_FOUND
      ));
    }
    return ResponseEntity.badRequest().body(new InvalidApiResponse(
            message.IDINVALID, HttpStatus.BAD_REQUEST
    ));
  }

  ResponseEntity<InvalidApiResponse> checkPost(PostRequest post){
    if (post.getUserId()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"UserID"),
              HttpStatus.BAD_REQUEST
      ));
    }
    if (!validate.checkDigit(post.getUserId())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
         message.IDINVALID, HttpStatus.BAD_REQUEST
      ));
    }
    UserResponse user = userService.getUserById(Integer.parseInt(post.getUserId()));
    if (user==null){
      return ResponseEntity.status(404).body(new InvalidApiResponse(
              message.notFound("User",Integer.parseInt(post.getUserId())),HttpStatus.NOT_FOUND
      ));
    }
    if (post.getTitle()==null&&post.getImages()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
         message.POSTINVALID, HttpStatus.BAD_REQUEST
      ));
    }
    if (post.getTitle()!=null){
      if (post.getTitle().equalsIgnoreCase("")&&post.getImages().size()==0){
        return ResponseEntity.badRequest().body(new InvalidApiResponse(
                message.POSTINVALID, HttpStatus.BAD_REQUEST
        ));
      }
    }
    return null;
  }

}

