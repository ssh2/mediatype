package com.freelance.media.controller.connection;

import com.freelance.media.model.connection.Following;
import com.freelance.media.model.connection.FollowingRequest;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.service.FollowingService;
import com.freelance.media.service.UserService;
import com.freelance.media.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = ApiConstant.baseUrl + "/connections")
public class ConnectionController {

  Message message;
  Validate validate;
  UserService userService;
  FollowingService followingService;

  @Autowired
  public void setFollowingService(FollowingService followingService) {
    this.followingService = followingService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setValidate(Validate validate) {
    this.validate = validate;
  }

  @Autowired
  public void setMessage(Message message) {
    this.message = message;
  }

  @PostMapping("/follow")
  public ResponseEntity follow(@RequestBody FollowingRequest request){
    ResponseEntity<InvalidApiResponse> invalidResponse = checkRequest(request);
    if (invalidResponse==null){
      Following following = followingService.follow(Integer.parseInt(request.getFollower()),
              Integer.parseInt(request.getUser()));
      if (following!=null){
        return ResponseEntity.ok(new BaseApiResponse<>(
                message.success(message.INSERTSUCCESS, "Following"),
                following, HttpStatus.CREATED

        ));
      }
      return ResponseEntity.internalServerError().body(new InvalidApiResponse(
              message.INTERNALERROR, HttpStatus.INTERNAL_SERVER_ERROR
      ));
    }
    return invalidResponse;
  }

  ResponseEntity<InvalidApiResponse> checkRequest(FollowingRequest request){
    if (request.getFollower()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"Follower"),
              HttpStatus.BAD_REQUEST
      ));
    }
    if (!validate.checkDigit(request.getFollower())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.IDINVALID,
              HttpStatus.BAD_REQUEST
      ));
    }
    UserResponse follower = userService.getUserById(Integer.parseInt(request.getFollower()));
    if (follower==null){
      return ResponseEntity.status(404).body(new InvalidApiResponse(
              message.notFound("User",Integer.parseInt(request.getFollower())),HttpStatus.NOT_FOUND
      ));
    }
    if (request.getUser()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"User"),
              HttpStatus.BAD_REQUEST
      ));
    }
    if (!validate.checkDigit(request.getFollower())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.IDINVALID,"Follower"),
              HttpStatus.BAD_REQUEST
      ));
    }
    UserResponse user = userService.getUserById(Integer.parseInt(request.getUser()));
    if (user==null){
      return ResponseEntity.status(404).body(new InvalidApiResponse(
              message.notFound("User",Integer.parseInt(request.getUser())),HttpStatus.NOT_FOUND
      ));
    }
    return null;
  }
}
