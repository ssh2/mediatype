package com.freelance.media.model.user;

import java.sql.Date;
import java.sql.Timestamp;

public class UserUpdate {
  private String displayName;
  private String gender;
  private String address;
  private String phoneNumber;
  private String imagePath;
  private Date dob;

  public UserUpdate() {
  }

  public UserUpdate(String displayName, String gender, String address, String phoneNumber, String imagePath, Date dob) {
    this.displayName = displayName;
    this.gender = gender;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.imagePath = imagePath;
    this.dob = dob;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  @Override
  public String toString() {
    return "UserRequest{" +
            ", displayName='" + displayName + '\'' +
            ", gender='" + gender + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", imagePath='" + imagePath + '\'' +
            ", dob=" + dob +
            '}';
  }
}
