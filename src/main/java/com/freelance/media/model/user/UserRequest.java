package com.freelance.media.model.user;

import java.sql.Date;
import java.sql.Timestamp;

public class UserRequest {

  private String userName;
  private String displayName;
  private String email;
  private String password;
  private String gender;
  private String address;
  private String phoneNumber;
  private String imagePath;
  private Date dob;

  public UserRequest() {
  }

  public UserRequest(String userName, String displayName, String email, String password, String gender, String address, String phoneNumber, String imagePath, Date dob) {
    this.userName = userName;
    this.displayName = displayName;
    this.email = email;
    this.password = password;
    this.gender = gender;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.imagePath = imagePath;
    this.dob = dob;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  @Override
  public String toString() {
    return "UserRequest{" +
            "userName='" + userName + '\'' +
            ", displayName='" + displayName + '\'' +
            ", email='" + email + '\'' +
            ", password='" + password + '\'' +
            ", gender='" + gender + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", imagePath='" + imagePath + '\'' +
            ", dob=" + dob +
            '}';
  }
}
