package com.freelance.media.model.connection;

public class Following {

  private int id;
  private int follower;
  private int user;

  public Following() {
  }

  public Following(int follower, int user){
    this.follower = follower;
    this.user = user;
  }

  public Following(int id, int follower, int user) {
    this.id = id;
    this.follower = follower;
    this.user = user;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getFollower() {
    return follower;
  }

  public void setFollower(int follower) {
    this.follower = follower;
  }

  public int getUser() {
    return user;
  }

  public void setUser(int user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "Following{" +
            "id=" + id +
            ", follower=" + follower +
            ", user=" + user +
            '}';
  }
}
