package com.freelance.media.model.post;

import java.sql.Timestamp;
import java.util.List;

public class Post {

  private int id;
  private int userId;
  private Timestamp date;
  private String title;
  private boolean isEdited;
  private String privacy;

  public Post() {
  }

  public Post(PostRequest post){
    this.userId = Integer.parseInt(post.getUserId());
    this.title = post.getTitle();
    this.privacy = post.getPrivacy();
  }

  public Post(int id, int userId, Timestamp date, String title, boolean isEdited, String privacy) {
    this.id = id;
    this.userId = userId;
    this.date = date;
    this.title = title;
    this.isEdited = isEdited;
    this.privacy = privacy;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public Timestamp getDate() {
    return date;
  }

  public void setDate(Timestamp date) {
    this.date = date;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isEdited() {
    return isEdited;
  }

  public void setEdited(boolean edited) {
    isEdited = edited;
  }

  public String getPrivacy() {
    return privacy;
  }

  public void setPrivacy(String privacy) {
    this.privacy = privacy;
  }

  @Override
  public String toString() {
    return "Post{" +
            "id=" + id +
            ", userId=" + userId +
            ", date=" + date +
            ", title='" + title + '\'' +
            ", isEdited=" + isEdited +
            ", privacy='" + privacy + '\'' +
            '}';
  }
}
