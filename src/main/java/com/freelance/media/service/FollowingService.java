package com.freelance.media.service;

import com.freelance.media.model.connection.Following;
import org.springframework.stereotype.Service;

@Service
public interface FollowingService {

  Following follow(int follower, int user);
}
