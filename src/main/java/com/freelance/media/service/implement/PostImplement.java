package com.freelance.media.service.implement;

import com.freelance.media.model.post.Post;
import com.freelance.media.model.post.PostRequest;
import com.freelance.media.model.post.PostResponse;
import com.freelance.media.model.user.UserOther;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.repository.PostRepository;
import com.freelance.media.service.PostService;
import com.freelance.media.util.ApiConstant;
import com.freelance.media.util.MethodReducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostImplement implements PostService {

  MethodReducer methodReducer;
  PostRepository postRepository;
  UserImplement userImplement;

  @Autowired
  public void setUserImplement(UserImplement userImplement) {
    this.userImplement = userImplement;
  }

  @Autowired
  public void setMethodReducer(MethodReducer methodReducer) {
    this.methodReducer = methodReducer;
  }

  @Autowired
  public void setPostRepository(PostRepository postRepository) {
    this.postRepository = postRepository;
  }

  @Override
  public PostResponse addPost(PostRequest newPost) {
    Post post = new Post(newPost);
    post.setDate(methodReducer.getTime());
    post.setEdited(false);
    if (newPost.getPrivacy()==null){
      post.setPrivacy(ApiConstant.PUBLIC);
    }
    Boolean isAdded = postRepository.addPost(post);
    if (isAdded){
      post.setId(post.getId());
      UserOther userResponse = new UserOther(userImplement.getUserById(post.getUserId()));
      PostResponse response = new PostResponse(post,userResponse);
      if (newPost.getImages().size() != 0) {
        for (String imagePath : newPost.getImages()){
          postRepository.addPostImage(post.getId(),imagePath);
        }
        response.setImages(newPost.getImages());
      }
      return response;
    }
    return null;
  }

  @Override
  public List<PostResponse> newFeed(int userId) {
    List<Post> postList = postRepository.newFeed(userId);
    List<PostResponse> responses = new ArrayList<>();
    for (Post post : postList){
      System.out.println(post);
      System.out.println(userImplement.getUserById(post.getUserId()));
      System.out.println("success");
      UserOther userResponse = new UserOther(userImplement.getUserById(post.getUserId()));
      PostResponse response = new PostResponse(post,userResponse);
      responses.add(response);
    }
    return responses;
  }
}
