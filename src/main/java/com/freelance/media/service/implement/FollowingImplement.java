package com.freelance.media.service.implement;

import com.freelance.media.model.connection.Following;
import com.freelance.media.model.connection.FollowingRequest;
import com.freelance.media.repository.ConnectionRepository;
import com.freelance.media.service.FollowingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FollowingImplement implements FollowingService {

  ConnectionRepository connectionRepository;

  @Autowired
  public void setConnectionRepository(ConnectionRepository connectionRepository) {
    this.connectionRepository = connectionRepository;
  }

  @Override
  public Following follow(int follower, int user) {
    Following following = new Following(follower, user);
    if (connectionRepository.follow(following)){
      following.setId(following.getId());
      return following;
    }
    return null;
  }
}
