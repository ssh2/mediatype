package com.freelance.media.service;

import com.freelance.media.model.post.PostRequest;
import com.freelance.media.model.post.PostResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {

  PostResponse addPost(PostRequest post);
  List<PostResponse> newFeed(int userId);
}
