package com.freelance.media.service;

import com.freelance.media.model.user.UserRequest;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.model.user.UserUpdate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

  List<UserResponse> getAllUsers();
  UserResponse getUserById(int id);
  UserResponse addNewUser(UserRequest newUser);
  UserResponse updateUser(UserUpdate updateUser, int id);
}
