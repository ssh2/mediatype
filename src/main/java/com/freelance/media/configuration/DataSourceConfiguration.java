package com.freelance.media.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

  @Bean
  public DataSource myPostgresDb() {
    DataSourceBuilder dataSource = DataSourceBuilder.create();
    dataSource.driverClassName("org.postgresql.Driver");
    dataSource.url("jdbc:postgresql://140.83.85.72:5432/media");
    dataSource.username("media");
    dataSource.password("media!@#");
    return dataSource.build();
  }
}
