package com.freelance.media.util;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class InvalidApiResponse {

  private String message;
  private HttpStatus status;
  private Timestamp time;
  private MethodReducer methodReducer = new MethodReducer();

  public InvalidApiResponse() {
  }

  public InvalidApiResponse(String message, HttpStatus status) {
    this.message = message;
    this.status = status;
    this.time = methodReducer.getTime();
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

  public Timestamp getTime() {
    return time;
  }

  public void setTime(Timestamp time) {
    this.time = time;
  }

  @Override
  public String toString() {
    return "InvalidApiResponse{" +
            "message='" + message + '\'' +
            ", status=" + status +
            ", time=" + time +
            '}';
  }
}
