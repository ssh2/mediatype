package com.freelance.media.util;

import com.freelance.media.service.implement.UserImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Validate {

  UserImplement userImplement;

  @Autowired
  public void setUserImplement(UserImplement userImplement) {
    this.userImplement = userImplement;
  }

  public boolean checkPassword(String password){
    return password.length() >= 6;
  }

  public boolean checkDigit(String number){
    Pattern digitPattern = Pattern.compile("\\d+");
    Matcher matcher = digitPattern.matcher(number);
    return matcher.matches();
  }

  public boolean checkAlphaNumeric(String string){
    String regex = "^[a-zA-Z0-9]+$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(string);
    return matcher.matches();
  }

  public boolean checkUsername(String userName){
    String regex = "^[a-z0-9]+$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(userName);
    return matcher.matches();
  }

  public boolean isEmail(String email) {
    String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
    java.util.regex.Matcher m = p.matcher(email);
    return m.matches();
  }

  public boolean isUsedUsername(String username){
    return userImplement.isUsedUsername(username);
  }
}
