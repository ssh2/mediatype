package com.freelance.media.util;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.regex.Pattern;

@Component
public class MethodReducer {

  private final Timestamp time = new Timestamp(System.currentTimeMillis());

  public Pattern digitPattern = Pattern.compile("\\d+");

  public Timestamp getTime() {
    return time;
  }

  public String encryptPassword(String rawPassword){
    int key = 6;
    String encryptedPassword = "";
    char[] chars = rawPassword.toCharArray();
    for(char c : chars){
      c += key;
      encryptedPassword = encryptedPassword + c;
    }
    return encryptedPassword;
  }
}
