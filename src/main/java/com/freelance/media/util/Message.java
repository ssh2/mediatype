package com.freelance.media.util;

import org.springframework.stereotype.Component;

@Component
public class Message {
  public final String REQUIRED = " is required!";
  public final String NOTNUMBER = " cannot contain number!";
  public final String ISUSED = " has taken! Please choose another ";
  public final String ALPHANUMERIC = " can contain only alphabets, numbers and with no space";
  public final String SHORTPASSWORD = " need atleast 6 letters!";
  public final String INVALID = " is invalid!";
  public final String GETSUCCESS = "GET SUCCESS";
  public final String IDINVALID = "Invalid ID! Please input digit only!";
  public final String INSERTSUCCESS = "INSERT SUCCESS";
  public final String UPDATESUCCESS = "UPDATE SUCCESS";
  public final String DELETESUCCESS = "DELETE SUCCESS";
  public final String INTERNALERROR = "Internal Server Error!";
  public final String POSTINVALID = "Invalid post! Post needs at least a tittle or a image!";

  public String notFound(String field, int id){
    return "There is no "+field+ " with ID: "+id+"!";
  }

  public String invalidMessage(String type, String field){
    String message= "";
    switch(type){
      case REQUIRED:
        message = field + REQUIRED;
        break;
      case NOTNUMBER:
        message = field + NOTNUMBER;
        break;
      case INVALID:
        message = field + INVALID;
        break;
      case ALPHANUMERIC:
        message = field + ALPHANUMERIC;
        break;
      case SHORTPASSWORD:
        message = field + SHORTPASSWORD;
        break;
      case ISUSED:
        message = field + ISUSED + field.toLowerCase() + "!";
        break;
    }
    return message;
  }

  public String success(String type, String field){
    String message = "";
    switch(type){
      case GETSUCCESS:
        message = "You have got " + field.toLowerCase() + " successfully!";
        break;
      case INSERTSUCCESS:
        message = "You have added new " + field.toLowerCase() + " successfully!";
        break;
      case UPDATESUCCESS:
        message = "You have updated " + field.toLowerCase() + " successfully!";
      case DELETESUCCESS:
        message = "You have deleted " + field.toLowerCase() + " successfully";
    }
    return message;
  }
}
